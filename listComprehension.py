# Let's learn about list comprehensions! 
# You are given three integers x,y and z representing the dimensions of a cuboid along with an integer . 
# Print a list of all possible coordinates given by (i, j, k) on a 3D grid where the sum of i + j + k is not equal to n . 
# Please use list comprehensions rather than multiple loops, as a learning exercise.

def listComprehension():
    x = int(input())
    y = int(input())
    z = int(input())
    n = int(input())
    oldList = [x,y,z]
    newList = [[i,j,k] for i in range(x+1) for j in range (y+1) for k in range(z+1) if i + j + k != n]
    print(newList)

# Given the participants' score sheet for your University Sports Day, you are required to find the runner-up score. 
# You are given n scores. 
# Store them in a list and find the score of the runner-up.

def findTheRunnerUpScore():
    n = int(input())
    arr = list(map(int, input().split()))
    arr.sort(reverse=True);
    maxNum = max(arr)
    for num in arr:
      if num < maxNum:
          print(num)
          break
    

findTheRunnerUpScore()








